﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float sensitivity;
    [SerializeField] float xAxisClamp = 0.0f; // Limite de rotation pour l'axe x de notre caméra
    [SerializeField] float jumpForce;
    [SerializeField] float fallMultiplier;
    [SerializeField] float lowJumpmultiplier;
    [SerializeField] float timerJump;
    Transform cam;
    Rigidbody myRigidbody;
    float actualTimeJump;


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = this.gameObject.transform.GetChild(0).GetComponent<Transform>();
        myRigidbody = GetComponent<Rigidbody>();
        actualTimeJump = 0.0f;
    }

    void Update()
    {

        // Déplace le joueur, retourne true si il est en train de se déplacer
        if (!playerMove())
        {
            // Fait des bruits de pas tant que le joueur se déplace
            FindObjectOfType<AudioManager>().Play("Bruit_de_pas");
        }

        // Rotation de la caméra
        cameraRotate();

        // Condition pour réaliser un saut
        if(myRigidbody.velocity[1] < 0.1f && myRigidbody.velocity[1] > -0.1f)
        {
            // Réalsie un saut
            jump();
            actualTimeJump = 0.0f;
        }
        else if(myRigidbody.velocity[1] < -0.01f)
        {
            // Si le joueur tombe, la gravité augmente pour plus de réalisme
            myRigidbody.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) *Time.deltaTime;
            actualTimeJump = 0.0f;
        }
        else if(myRigidbody.velocity[1] > 0.01f && Input.GetKey(KeyCode.Space))
        {
            // Condition pour le nuancier de saut
            if (actualTimeJump < timerJump)
            {
                actualTimeJump++;
                myRigidbody.velocity -= Vector3.up * Physics.gravity.y * (lowJumpmultiplier - 1) * Time.deltaTime;
            }
        }
    }

    // Fonction permettant de gerer le deplacement du joueur, il retourne true si il est en train de se déplacer
    bool playerMove()
    {
        // Récupère le déplacement sur l'axe Vertical et Horizontal
        float moveY = speed * Input.GetAxis("Vertical");
        float moveX = speed * Input.GetAxis("Horizontal");

        // Déplace le joueur
        //myRigidbody.velocity = new Vector3(moveX,myRigidbody.velocity[1],moveY);
        transform.Translate(moveX, 0, moveY);

        // Si il y a un dépalcement sur l'axe Horizontal ou Vertical, retourne true
        return (moveY != 0 || moveX != 0) ? true : false;
    }

    // Fonction de rotation de la caméra
    void cameraRotate()
    {
        float cameraX = sensitivity * Input.GetAxis("Mouse X") * Time.deltaTime;
        float cameraY = sensitivity * Input.GetAxis("Mouse Y") * Time.deltaTime;

        xAxisClamp += cameraY;

        // Limite de la caméra
        if (xAxisClamp > 90.0f)
        {
            xAxisClamp = 90.0f;
            cameraY = 0;
            clampXAxisRotation(270.0f);
        }
        else if (xAxisClamp < -90.0f)
        {
            xAxisClamp = -90.0f;
            cameraY = 0;
            clampXAxisRotation(90.0f);        
        }

        cam.Rotate(Vector3.left * cameraY);
        transform.Rotate(Vector3.up * cameraX);
    }

    void clampXAxisRotation(float value)    // value une valeur en degres
    {
        Vector3 eulerRotation = cam.eulerAngles;
        eulerRotation.x = value;                        // On injècte dans ce Vector3 notre value
        cam.eulerAngles = eulerRotation;          // On applique notre Vector3 a l'angle
    }

    // Fonction permettant le fait de sauter
    void jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            myRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }

    
}
